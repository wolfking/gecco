package com.geccocrawler.gecco.listener;

import com.geccocrawler.gecco.GeccoEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 简单的引擎时间兼容实现类，可以继承该类覆盖需要的方法
 *
 * @author LiuJunGuang
 */
public abstract class SimpleEventListener implements EventListener {

    protected Logger logger = LoggerFactory.getLogger(getClass());
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    /*
     * (non-Javadoc)
     *
     * @see com.geccocrawler.gecco.listener.EventListener#onStart(com.geccocrawler.gecco.GeccoEngine)
     */
    @Override
    public void onStart(GeccoEngine ge) {
        logger.info("engin {} start at {}", ge.getEngineId(), sdf.format(new Date()));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.geccocrawler.gecco.listener.EventListener#onPause(com.geccocrawler.gecco.GeccoEngine)
     */
    @Override
    public void onPause(GeccoEngine ge) {
        logger.info("engin {} pause at {}", ge.getEngineId(), sdf.format(new Date()));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.geccocrawler.gecco.listener.EventListener#onRestart(com.geccocrawler.gecco.GeccoEngine)
     */
    @Override
    public void onRestart(GeccoEngine ge) {
        logger.info("engin {} restart at {}", ge.getEngineId(), sdf.format(new Date()));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.geccocrawler.gecco.listener.EventListener#onStop(com.geccocrawler.gecco.GeccoEngine)
     */
    @Override
    public void onStop(GeccoEngine ge) {
        logger.info("engin {} stop at {}", ge.getEngineId(), sdf.format(new Date()));
    }

}
