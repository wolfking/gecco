package com.geccocrawler.gecco.downloader;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;
import java.util.Collections;
import java.util.List;

/**
 * 随机获取userAgent，通过在classpath根目录下放置userAgents文件，配置多个userAgent，随机选择，如果希望某个ua概率较高请配置多个
 *
 * @author huchengyi
 */
public class UserAgent {

    private static Logger logger = LoggerFactory.getLogger(UserAgent.class);

    private static final String DEFAULT_USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36";

    private static final String DEFAULT_MOBILE_USER_AGENT = "Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A403 Safari/8536.25";

    private static List<String> userAgents = null;

    static {
        try {
            URL url = Resources.getResource("userAgents");
            File file = new File(url.getPath());
            userAgents = Files.readLines(file, Charsets.UTF_8);
        } catch (Exception ex) {

            String agents = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)\n" +
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2)\n" +
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)\n" +
                    "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT)\n" +
                    "Mozilla/5.0 (Windows; U; Windows NT 5.2) Gecko/2008070208 Firefox/3.0.1\n" +
                    "Mozilla/5.0 (Windows; U; Windows NT 5.1) Gecko/20070309 Firefox/2.0.0.3\n" +
                    "Mozilla/5.0 (Windows; U; Windows NT 5.1) Gecko/20070803 Firefox/1.5.0.12\n" +
                    "Opera/9.27 (Windows NT 5.2; U; zh-cn)\n" +
                    "Opera/8.0 (Macintosh; PPC Mac OS X; U; en)\n" +
                    "Mozilla/5.0 (Macintosh; PPC Mac OS X; U; en) Opera 8.0\n" +
                    "Mozilla/5.0 (Windows; U; Windows NT 5.2) AppleWebKit/525.13 (KHTML, like Gecko) Version/3.1 Safari/525.13\n" +
                    "Mozilla/5.0 (Windows; U; Windows NT 5.2) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.2.149.27 Safari/525.13\n" +
                    "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.12) Gecko/20080219 Firefox/2.0.0.12 Navigator/9.0.0.6\n" +
                    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; 360SE)\n" +
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; 360SE)\n" +
                    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ;  QIHU 360EE)\n" +
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; Maxthon/3.0)\n" +
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; TencentTraveler 4.0; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) )\n" +
                    "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.5 Safari/534.55.3";
            logger.warn("userAgents is not loaded,it will use {}",agents);
            userAgents = Lists.newArrayList(agents.split("\n"));
        }
    }

    private static List<String> mobileUserAgents = null;

    static {
        try {
            URL url = Resources.getResource("mobileUserAgents");
            File file = new File(url.getPath());
            mobileUserAgents = Files.readLines(file, Charsets.UTF_8);
        } catch (Exception ex) {
        }
    }

    public static String getUserAgent(boolean isMobile) {
        if (isMobile) {
            if (mobileUserAgents == null || mobileUserAgents.size() == 0) {
                return DEFAULT_MOBILE_USER_AGENT;
            }
            Collections.shuffle(mobileUserAgents);
            return mobileUserAgents.get(0);
        } else {
            if (userAgents == null || userAgents.size() == 0) {
                return DEFAULT_USER_AGENT;
            }
            Collections.shuffle(userAgents);
            return userAgents.get(0);
        }
    }

}
