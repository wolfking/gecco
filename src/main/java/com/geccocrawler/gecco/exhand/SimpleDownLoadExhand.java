package com.geccocrawler.gecco.exhand;

import com.geccocrawler.gecco.annotation.DownloadExHandName;
import com.geccocrawler.gecco.request.HttpRequest;
import com.geccocrawler.gecco.response.HttpResponse;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DownloadExHandName("simpleDownLoadExhand")
public class SimpleDownLoadExhand implements DownLoadExhand {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void handDownloadExcetpion(HttpRequest request, HttpResponse response, Exception exception) {
        logger.error("\ndownload ex,message is [{}],url is [{}],stack trace is\n", exception.getMessage(), request.getUrl(), ExceptionUtils.getStackTrace(exception));
    }
}
