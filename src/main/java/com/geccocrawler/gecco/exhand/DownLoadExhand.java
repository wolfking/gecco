package com.geccocrawler.gecco.exhand;

import com.geccocrawler.gecco.request.HttpRequest;
import com.geccocrawler.gecco.response.HttpResponse;

/**
 * 处理下载异常
 */
public interface DownLoadExhand {
    void handDownloadExcetpion(HttpRequest request, HttpResponse response, Exception exception);
}
