package com.geccocrawler.gecco.exhand;

/**
 * 下载异常的factory
 */
public interface DownLoadExhandFactory {

    /**
     * 根据name 获取异常控制器
     *
     * @param name
     * @return
     */
    DownLoadExhand getDownLoadExhand(String name);
}
