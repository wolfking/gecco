package com.geccocrawler.gecco.exhand;

import com.geccocrawler.gecco.annotation.DownloadExHandName;
import com.google.common.collect.Maps;
import org.reflections.Reflections;

import java.util.Map;
import java.util.Set;

public class DefaultDownLoadExHandFactory implements DownLoadExhandFactory {

    private Map<String, DownLoadExhand> downLoadExhandMap;

    public DefaultDownLoadExHandFactory(Reflections reflections) {
        this.downLoadExhandMap = Maps.newHashMap();
        Set<Class<?>> downLoadExhandClasses = reflections.getTypesAnnotatedWith(DownloadExHandName.class);
        for (Class<?> downLoadExhandClasse : downLoadExhandClasses) {
            DownloadExHandName downloadExHandName = downLoadExhandClasse.getAnnotation(DownloadExHandName.class);
            try {
                downLoadExhandMap.put(downloadExHandName.value(), (DownLoadExhand) downLoadExhandClasse.newInstance());
            } catch (Exception ex) {
                ex.printStackTrace(System.out);
            }
        }
    }

    @Override
    public DownLoadExhand getDownLoadExhand(String name) {
        return downLoadExhandMap.get(name);
    }
}
