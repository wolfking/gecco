package com.geccocrawler.gecco.pipeline;

import com.alibaba.fastjson.JSON;
import com.geccocrawler.gecco.annotation.PipelineName;
import com.geccocrawler.gecco.spider.SpiderBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 打印到slf4j 和sout的处理类
 */
@PipelineName("consolePipeline")
public class ConsolePipeline implements Pipeline<SpiderBean> {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void process(SpiderBean bean) {
        logger.info("{}", JSON.toJSONString(bean));
    }

}
