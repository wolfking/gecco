package com.geccocrawler.gecco.scheduler;

import com.geccocrawler.gecco.request.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 阻塞队列
 *
 * @author huchengyi
 */
public class StartScheduler implements Scheduler {

    private Logger logger = LoggerFactory.getLogger(getClass());
    private LinkedBlockingQueue<HttpRequest> startQueue = new LinkedBlockingQueue<>();


    @Override
    public HttpRequest out() {
        HttpRequest request = null;
        try {
            request = startQueue.poll(1, TimeUnit.SECONDS);
        } catch (Exception e) {
            logger.warn("queue poll 1s null");
        }
        return request;
    }

    @Override
    public void into(HttpRequest request) {
        try {
            startQueue.put(request);
        } catch (InterruptedException e) {
            logger.error("queue put error",e);
        }
    }

}
