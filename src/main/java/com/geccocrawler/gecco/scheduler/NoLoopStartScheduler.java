package com.geccocrawler.gecco.scheduler;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import com.geccocrawler.gecco.request.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 不需要循环抓取的start队列
 * 
 * @author huchengyi
 *
 */
public class NoLoopStartScheduler implements Scheduler {

	private Logger logger  = LoggerFactory.getLogger(getClass());
	private LinkedBlockingQueue<HttpRequest> queue = new LinkedBlockingQueue<>();
	

	@Override
	public HttpRequest out() {
		HttpRequest request =null;
		try{
			request= queue.poll(1, TimeUnit.SECONDS);
		}catch (Exception e){
			logger.warn("queue poll 1s null");
		}
		return request;
	}

	@Override
	public void into(HttpRequest request) {
		queue.offer(request);
	}

}
