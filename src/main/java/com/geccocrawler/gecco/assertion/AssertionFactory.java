package com.geccocrawler.gecco.assertion;

public interface AssertionFactory {
    Assertion getAssertion(String name);
}
