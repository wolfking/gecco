package com.geccocrawler.gecco.assertion;

/**
 * 响应的断言
 */
public interface Assertion extends Cloneable{

    boolean judge(String responseBody);

    void setAssertionRule(String assertionRule);

    Assertion cloneAssertion();
}
