package com.geccocrawler.gecco.assertion;

import com.geccocrawler.gecco.annotation.AssertionName;
import com.google.common.collect.Maps;
import org.reflections.Reflections;

import java.util.Map;
import java.util.Set;

public class DefaultAssertionFactory implements AssertionFactory {

    private Map<String, Assertion> assertionMap;

    public DefaultAssertionFactory(Reflections reflections) {
        this.assertionMap = Maps.newHashMap();
        Set<Class<?>> assertionClasses = reflections.getTypesAnnotatedWith(AssertionName.class);
        for (Class<?> assertionClass : assertionClasses) {
            AssertionName assertionName = assertionClass.getAnnotation(AssertionName.class);
            try {
                assertionMap.put(assertionName.value(), (Assertion) assertionClass.newInstance());
            } catch (Exception ex) {
                ex.printStackTrace(System.out);
            }
        }
    }

    @Override
    public Assertion getAssertion(String name) {
        Assertion assertion =  assertionMap.get(name);
        return assertion.cloneAssertion();
    }
}
