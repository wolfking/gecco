package com.geccocrawler.gecco.assertion;

public abstract class AbstractAssertion implements Assertion {
    private String assertionRule;

    @Override
    public void setAssertionRule(String assertionRule) {
        this.assertionRule = assertionRule;
    }

    public String getAssertionRule() {
        return assertionRule;
    }
}
