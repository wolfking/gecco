package com.geccocrawler.gecco.assertion;

import com.geccocrawler.gecco.annotation.AssertionName;
import org.apache.commons.lang3.StringUtils;

@AssertionName("simpleContainsAssertion")
public class SimpleContainsAssertion extends AbstractAssertion {

//    private Logger logger  = LoggerFactory.getLogger(getClass());

    @Override
    public boolean judge(String responseBody) {
        return StringUtils.isEmpty(getAssertionRule()) || StringUtils.isNotEmpty(responseBody) && responseBody.contains(getAssertionRule());
    }

    @Override
    public Assertion cloneAssertion() {
        try {
            return Assertion.class.cast(clone());
        } catch (Exception ee) {
            throw new RuntimeException("clone assertion error", ee);
        }
    }
}
