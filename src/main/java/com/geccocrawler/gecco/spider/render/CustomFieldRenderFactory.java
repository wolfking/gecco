package com.geccocrawler.gecco.spider.render;

import com.geccocrawler.gecco.annotation.FieldRenderName;
import com.google.common.collect.Maps;
import org.reflections.Reflections;

import java.util.Map;
import java.util.Set;

public class CustomFieldRenderFactory {
	
	private Map<String, CustomFieldRender> map;
	
	public CustomFieldRenderFactory(Reflections reflections) {
		this.map = Maps.newHashMap();
		Set<Class<?>> classes = reflections.getTypesAnnotatedWith(FieldRenderName.class);
		for(Class<?> clazz : classes) {
			FieldRenderName fieldRenderName = clazz.getAnnotation(FieldRenderName.class);
			try {
				map.put(fieldRenderName.value(), (CustomFieldRender)clazz.newInstance());
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public CustomFieldRender getCustomFieldRender(String name) {
		return map.get(name);
	}

}
