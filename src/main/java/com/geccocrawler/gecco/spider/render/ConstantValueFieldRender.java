package com.geccocrawler.gecco.spider.render;

import com.geccocrawler.gecco.annotation.ConstantValue;
import com.geccocrawler.gecco.request.HttpRequest;
import com.geccocrawler.gecco.response.HttpResponse;
import com.geccocrawler.gecco.spider.SpiderBean;
import net.sf.cglib.beans.BeanMap;
import org.reflections.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.Set;

/**
 * 动态生成的静态常亮的注解
 */
public class ConstantValueFieldRender implements FieldRender {

    @Override
    @SuppressWarnings("unchecked")
    public void render(HttpRequest request, HttpResponse response, BeanMap beanMap, SpiderBean bean) {
        Set<Field> requestFields = ReflectionUtils.getAllFields(bean.getClass(), ReflectionUtils.withAnnotation(ConstantValue.class));
        for (Field field : requestFields) {
            beanMap.put(field.getName(), field.getAnnotation(ConstantValue.class).value());
        }
    }
}
