package com.geccocrawler.gecco.spider.render;

import com.geccocrawler.gecco.spider.SpiderBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 注入bean的多个filed异常
 * 
 * @author huchengyi
 *
 */
public class RenderException extends Exception {
	
	private static Logger log = LoggerFactory.getLogger(RenderException.class);

	private static final long serialVersionUID = 5034687491589622988L;

	private Class<? extends SpiderBean> spiderBeanClass;
	
	public RenderException(String message, Class<? extends SpiderBean> spiderBeanClass) {
		super(message);
		this.spiderBeanClass = spiderBeanClass;
	}
	
	public RenderException(String message, Class<? extends SpiderBean> spiderBeanClass, FieldRenderException cause) {
		super(message, cause);
		this.spiderBeanClass = spiderBeanClass;
	}

	public Class<? extends SpiderBean> getSpiderBeanClass() {
		return spiderBeanClass;
	}

	public void setSpiderBeanClass(Class<? extends SpiderBean> spiderBeanClass) {
		this.spiderBeanClass = spiderBeanClass;
	}

	public static void log(String message, Class<? extends SpiderBean> spiderBeanClass, Throwable cause) {
		log.error(spiderBeanClass.getName() + " render error : " + message);
		if(cause != null) {
			log.error(message, cause);
		}
	}
	
	public static void log(String message, Class<? extends SpiderBean> spiderBeanClass) {
		log(message, spiderBeanClass, null);
	}
}
