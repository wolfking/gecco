package com.geccocrawler.gecco.annotation;

import java.lang.annotation.*;

/**
 * 相应的HttpResponse
 *
 * @author zhao.weiwei
 */
@Inherited
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Response {

}
