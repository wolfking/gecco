package com.geccocrawler.gecco.annotation;

import java.lang.annotation.*;

/**
 * 相应的body体
 * 字段类型为String
 * @author zhao.weiwei
 */
@Inherited
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ResponseBody {

}
