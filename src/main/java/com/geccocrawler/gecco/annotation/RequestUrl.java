package com.geccocrawler.gecco.annotation;

import java.lang.annotation.*;

/**
 *  请求的url
 *  字段类型为String
 * @author zhao.weiwei
 *
 */
@Inherited
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestUrl {
	
}
